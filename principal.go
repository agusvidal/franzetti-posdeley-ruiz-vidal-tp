package main

import (
	"bufio"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"os"
)

type consumo struct {
	NroTarjeta  string
	Cod         string
	NroComercio string
	Monto       string
}

//FUNCIONES

func createDatabase() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Query(`drop database if exists tarjetasdecredito`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`create database tarjetasdecredito`)
	if err != nil {
		log.Fatal(err)
	}
}

func crearTablas(db *sql.DB, err error) {
	_, err = db.Exec(`
	create table cliente(                                                   
    	nro_cliente  int,                                                   
    	nombre  text,                                                       
    	apellido text,                                                      
    	domicilio text,                                                     
    	telefono char(12)                                                   
	);

	create table tarjeta(
   		nro_tarjeta  char(16),
    	nro_cliente        int,
    	valida_desde       char(6),
    	valida_hasta    char(6),
    	cod_seguridad   char(4),
    	limite_compra   decimal(8,2),
    	estado   char(10)
	);

	create table comercio(
    	nro_comercio   int,
    	nombre      text,
    	domicilio   text,
    	codigo_postal   char(8),
    	telefono    char(12)
	);

	create table compra(
    	nro_operacion   int,
    	nro_tarjeta char(16),
    	nro_comercio    int,
    	fecha   timestamp,
    	monto   decimal(7,2),
    	pagado  boolean
	);

	create table rechazo(
    	nro_rechazo     int,
    	nro_tarjeta char(16),
    	nro_comercio    int,
    	fecha   timestamp,
    	monto   decimal(7,2),
    	motivo  text
);

	create table cierre(
    	anio    int,
    	mes int,
    	terminacion int,
    	fecha_inicio    date,
    	fecha_cierre    date,
    	fecha_vto   date
	);

	create table cabecera(
    	nro_resumen     int,
    	nombre  text,
    	apellido    text,
    	domicilio   text,
    	nro_tarjeta char(16),
    	desde   date,
    	hasta   date,
    	vence   date,
    	total   decimal(8,2)
	);

	create table detalle(
    	nro_resumen     int,
    	nro_linea   int,
    	fecha   date,
    	nombre_comercio text,
    	monto   decimal(7,2)
	);

	create table alerta(
    	nro_alerta  int,
    	nro_tarjeta char(16),
    	fecha   timestamp,
    	nro_rechazo int,
    	cod_alerta  int,    --0:rechazo, 1:compra 1min, 5:compra 5min, 32:límite
    	descripcion text
	);

	create table consumo(
    	nro_tarjeta char(16),
    	cod_seguridad   char(4),
    	nro_comercio    int,
    	monto   decimal(7,2)
	);
	`)
	if err != nil {
		log.Fatal(err)
	}
}

func crearPksYFks(db *sql.DB, err error) {
	_, err = db.Exec(`

	alter table cliente add constraint cliente_pk  primary key (nro_cliente);
                                                                        
	alter table tarjeta add constraint tarjeta_pk primary key (nro_tarjeta);
                                                                        
	alter table comercio add constraint comercio_pk primary key (nro_comercio);
                                                                        
	alter table compra add constraint compra_pk primary key (nro_operacion);
                                                                        
	alter table rechazo add constraint rechazo_pk primary key (nro_rechazo);
                                                                        
	alter table cierre add constraint cierre_pk primary key (anio,mes,terminacion);
                                                                        
	alter table cabecera add constraint cabecera_pk primary key (nro_resumen);
                                                                        
	alter table detalle add constraint detalle_pk primary key (nro_resumen,nro_linea);
                                                                        
	alter table alerta add constraint alerta_pk primary key (nro_alerta);
	
	alter table tarjeta add constraint tarjeta_nro_cliente_fk foreign key (nro_cliente) references cliente (nro_cliente);

	alter table compra add constraint compra_nro_tarjeta_fk foreign key (nro_tarjeta) references tarjeta (nro_tarjeta);

	alter table compra add constraint compra_nro_comercio_fk foreign key (nro_comercio) references comercio (nro_comercio);

	`)
}

func crearTarjetaNoValida(db *sql.DB, err error) {
	_, err = db.Exec(`
		create or replace function tarjetaNoValida(nro varchar(16)) returns boolean as $$
	declare                                                                 
																			
	resultado record;                                                       
																																			 
	begin                                                                   
																			
	select * into resultado from tarjeta where nro_tarjeta = nro;            
	if found then                                                           
		if resultado.estado = 'vigente' then                                
			return false;                                                    
		end if;                                                             
	end if;                                                                 
																			
	return true;                                                           
																			
	end;                                                                    
	$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}

}

func crearCodInvalido(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function codInvalido(nro_tarj varchar(16),cod varchar(4)) returns boolean as $$ 
	declare

	resultado record;

	begin

	select * into resultado from tarjeta where nro_tarjeta=nro_tarj;
	if found then
		if resultado.cod_seguridad<>cod then
			return true;
		end if;
	end if;

	return false;

	end;
	$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}

}

func crearMontoNoPermitido(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function montoNoPermitido(nro_tarj varchar(16),monto_a_pagar decimal(7,2))returns boolean as $$
	declare

	limite decimal(8,2);
	pendientes_de_pago decimal(8,2);

	begin
	
	select limite_compra into limite from tarjeta where  tarjeta.nro_tarjeta  = nro_tarj ;
	select sum(monto) into pendientes_de_pago from compra where pagado = false and compra.nro_tarjeta = nro_tarj; 
	
	if pendientes_de_pago is null then
		pendientes_de_pago := 0.0;
	end if;
		
	if pendientes_de_pago+monto_a_pagar > limite then

		return true;

	else

		return false;

	end if;

	end;
	$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
}

func crearTarjetaSuspendida(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function tarjetaSuspendida(nro_tarj varchar(16)) returns boolean as $$
	declare                                                                 
																			
	resultado record;                                                       
																			
	begin                                                                   
																			
	select * into resultado from tarjeta where tarjeta.nro_tarjeta=nro_tarj;          
																			
	if found then                                                           
			if resultado.estado = 'suspendida' then                         
					return true;                                            
																			
				end if;                                                     
	end if;                                                                 
																			
																			
	return false;                                                           
																			
	end;                                                                    
	$$ language plpgsql; `)
	if err != nil {
		log.Fatal(err)
	}
}

func crearGenerarResumen(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function generarResumen(nro_tarj varchar(16),periodo int) returns void as $$
	declare
		pk_resumen int;
		pk_linea int;
		cliente record;
		tarjeta record;
		compras record;
		cierre record;
		ultimo_digito char;
		total_resumen decimal(8,2);
		nombre_comercio text;
																			
	begin

		select count(nro_resumen) into pk_resumen from cabecera;
		pk_resumen := pk_resumen + 1;


		select * into cliente from cliente where nro_cliente in (
			select nro_cliente from tarjeta where nro_tarjeta = nro_tarj); -- encuentro el cliente

		select * into tarjeta from tarjeta where nro_tarjeta = nro_tarj;  -- encuentro la tarjeta

		select into ultimo_digito right(tarjeta.nro_tarjeta,1);   -- guardo la terminacion de la tarjeta

		select * into cierre from cierre c where c.mes = periodo and c.terminacion = (select(ultimo_digito :: int)); -- encuentro cierre

		pk_linea := 0;
		total_resumen := 0;
		
		if cierre.fecha_inicio not in (select desde from cabecera) and cierre.fecha_cierre not in (select hasta from cabecera) then

			for compras in select * from compra loop
			
				if compras.nro_tarjeta = tarjeta.nro_tarjeta and cierre.fecha_inicio<=compras.fecha and cierre.fecha_cierre>=compras.fecha 
				   and compras.pagado = false then

					select into nombre_comercio from comercio c where c.nro_comercio = compras.nro_comercio; 
					
					insert into detalle values(pk_resumen,
											   pk_linea,
											   compras.fecha,
											   nombre_comercio,
											   compras.monto);

					total_resumen := total_resumen + compras.monto;
					
					pk_linea := pk_linea + 1;

				end if;
			
			end loop;
			
			
			
			insert into cabecera values(pk_resumen,
										cliente.nombre,
										cliente.apellido,
										cliente.domicilio,
										nro_tarj,
										cierre.fecha_inicio,
										cierre.fecha_cierre,
										cierre.fecha_vto,
										total_resumen);
		end if;
																			
	end;                                                                    
	$$ language plpgsql; 
		`)
	if err != nil {
		log.Fatal(err)
	}
}

func crearTarjetaVencida(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function tarjetaVencida(nro_tarj varchar(16))returns boolean as $$
		declare                                                                 
			resultado record;                                                       
			fecha timestamp;                                                        
			mes_vencimiento varchar(2);                                             
			anio_vencimiento varchar(4);                                            
			anio_actual varchar(4);                                                 
			mes_actual varchar(2);                                                  
                                                                        
	begin                                                                   
    	select * into resultado from tarjeta where nro_tarjeta=nro_tarj;    
                                                                        
    	if found then                                                       
        	fecha:=current_timestamp;                                       
        	select extract(year from fecha) into anio_actual;               
        	select extract(month from fecha) into mes_actual;               
        	anio_vencimiento:=substring(resultado.valida_hasta from 1 for 4);
        	mes_vencimiento:= substring(resultado.valida_hasta from 5 for 6);
        	if anio_actual=anio_vencimiento and mes_actual>=mes_vencimiento then
            	return true;                                                
        	end if;                                                         
        	if anio_actual>anio_vencimiento then                            
            	return true;                                                
        	end if;                                                         
		end if;                                                                 
		return false;                                                           
	end;                                                                    
	$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
}

func crearAutorizacionDeCompra(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function autorizacionDeCompra(nro_tarj varchar(16),cod varchar(4), nro_comercio int, monto decimal(8,2))returns boolean as $$
		declare

		pk_rechazo int;
		nro_compra int;
		tarjeta_suspendida_ok bool;
		monto_ok bool;
		codigo_ok bool;
		tarjeta_invalida_ok bool;
		tarjeta_vencida_ok bool;
		begin

		select count (*) into pk_rechazo from rechazo;
		pk_rechazo := pk_rechazo+1;
		select count (*) into nro_compra from compra;
		nro_compra := nro_compra +1;

		select tarjetaSuspendida(nro_tarj) into tarjeta_suspendida_ok;
		select montoNoPermitido(nro_tarj, monto) into monto_ok;
		select codInvalido (nro_tarj, cod) into codigo_ok;
		select tarjetaNoValida (nro_tarj) into tarjeta_invalida_ok;
		select tarjetaVencida (nro_tarj) into tarjeta_vencida_ok;

		if tarjeta_vencida_ok then 
			insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'Plazo de vigencia expirado');
			return false;
		end if;

		if tarjeta_suspendida_ok then
			insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'La tarjeta se encuentra suspendida');
			return false;
		end if;

		if monto_ok then
			insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'Supera el limite de tarjeta');
			return false;
		end if;

		if codigo_ok then 
			insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'Codigo de seguridad invalido');
			return false;
		end if;

		if tarjeta_invalida_ok then 
			insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'Tarjeta no valida o no vigente');
			return false;
		end if;

			insert into compra values(nro_compra,nro_tarj,nro_comercio,current_timestamp, monto, false);
			return true;

		end;

	$$ language plpgsql;`)

	if err != nil {
		log.Fatal(err)
	}
}

func crearEliminarPKs(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function EliminarPKs() returns void as $$

	begin

	alter table tarjeta drop constraint tarjeta_nro_cliente_fk ;
	alter table compra drop constraint compra_nro_tarjeta_fk;
	alter table compra drop constraint compra_nro_comercio_fk;


	alter table cliente drop constraint cliente_pk;
	alter table tarjeta drop constraint tarjeta_pk;
	alter table comercio drop constraint comercio_pk ;
	alter table compra drop constraint compra_pk;
	alter table rechazo drop constraint rechazo_pk ;
	alter table cierre drop constraint cierre_pk;
	alter table cabecera drop constraint cabecera_pk ;
	alter table detalle drop constraint detalle_pk ;
	alter table alerta drop constraint alerta_pk ;

	end;                                                                    
                                                                        
	$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
}

//TRIGGERS

func crearTriggerRechazo(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function triggerRechazo() returns trigger as $$

declare

pk_alerta int;

begin 
	
	select count(nro_alerta) into pk_alerta from alerta;
	pk_alerta := pk_alerta + 1;
	
	
	insert into alerta values(pk_alerta,new.nro_tarjeta,new.fecha,new.nro_rechazo,0,new.motivo);
	return new;

end;
$$ language plpgsql;

create trigger rechazo_trg
before insert on rechazo
for each row
execute procedure triggerRechazo();`)

	if err != nil {
		log.Fatal(err)
	}
}

func crearTriggerComprasEnCincoMinutos(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function triggerComprasEnCincoMinutos() returns trigger as $$
                                                                        
	declare                                                                 
                                                                        
    	pk_alerta int;                                                      
    	segundos_diferencias int;                                           
    	fecha_actual timestamp;                                             
    	compras int;                                                        
                                                                        
	begin                                                                   
                                                                                                                                       
    	select count(nro_alerta) into pk_alerta from alerta;                
    	pk_alerta := pk_alerta + 1;                                         
    	fecha_actual:= new.fecha;
		
		select count (distinct (comercio.codigo_postal)) into compras from comercio where comercio.codigo_postal in (
        select comercio.codigo_postal from compra,comercio
            where compra.nro_comercio = comercio.nro_comercio and
            compra.nro_tarjeta = new.nro_tarjeta and
            extract(year from fecha_actual) = extract(year from fecha) and
            extract(month from fecha_actual) = extract(month from fecha) and
            extract(day from fecha_actual)= extract(day from fecha) and
            new.nro_tarjeta = compra.nro_tarjeta and
            ((extract (hour from fecha_actual)*3600 + extract (minute from fecha_actual)*60 + extract(second from fecha_actual))-
            (extract (hour from fecha)*3600+ extract(minute from fecha)*60 + extract(second from fecha)))<301
            group by comercio.codigo_postal having count(*) = 1);

			if compras>1 then
        		insert into alerta values(pk_alerta, new.nro_tarjeta, new.fecha, null, 5, 'se hizo mas de una compra en cinco minutos');
    		end if;
        	return new;

	end;
	$$ language plpgsql;
	
	create trigger comprasProximasACincoMinutos_trg
	after insert on compra
	for each row
	execute procedure triggerComprasEnCincoMinutos();
	`)
	if err != nil {
		log.Fatal(err)
	}
}

func crearTriggerComprasEnUnMinuto(db *sql.DB, err error) {
	_, err = db.Exec(`
	create or replace function triggerComprasEnUnMinuto() returns trigger as $$       
																			
	declare                                                                 
	   
		fecha_actual timestamp;
		compras	 int;
		pk_alerta int;
		
																			
	begin   

		select count(nro_alerta) into pk_alerta from alerta;                  
		pk_alerta := pk_alerta + 1;

		fecha_actual := new.fecha;
		
		select count(*) into compras from comercio, compra
		where compra.nro_comercio = comercio.nro_comercio and 
		extract(year from fecha_actual) = extract(year from compra.fecha) and extract(month from fecha_actual) = extract(month from compra.fecha) and 
		extract(day from fecha_actual)= extract(day from compra.fecha) and 
		new.nro_tarjeta = compra.nro_tarjeta and 
		((extract (hour from fecha_actual)*3600 + extract (minute from fecha_actual)*60+ extract(second from fecha_actual))-
		(extract (hour from fecha)*3600+ extract(minute from fecha)*60 + extract(second from fecha)))<61 and
		compra.nro_tarjeta=new.nro_tarjeta and 
		compra.nro_comercio <> new.nro_comercio and
		(select codigo_postal from comercio c where c.nro_comercio = new.nro_comercio) = (select codigo_postal from comercio c where c.nro_comercio = compra.nro_comercio);

		if compras>0 then
			insert into alerta values(pk_alerta, new.nro_tarjeta, new.fecha, null, 1, 'se hizo mas de una compra en un minuto'); 
		end if;
			return new;                                                         
																			
	end;                                                                    
	$$ language plpgsql;                                                    
																			
	create trigger comprasProximasAUnMin_trg                                              
	after insert on compra                                                
	for each row                                                            
	execute procedure triggerComprasEnUnMinuto();
		`)
	if err != nil {
		log.Fatal(err)
	}
}

func agregarTriggerRechazosDiarios(db *sql.DB, err error) {
	_, err = db.Exec(`
		create or replace function alertaRechazosDiarios() returns trigger as $$

		declare
		rechazo_registrado int;
		pk_alerta int;
		fecha_actual timestamp;
		estado_tarjeta varchar(15);
		begin

		select count(nro_alerta) into pk_alerta from alerta;
		pk_alerta := pk_alerta + 1;
		fecha_actual := new.fecha;
			
		select count(*) into rechazo_registrado from rechazo where rechazo.nro_tarjeta = new.nro_tarjeta and 
		extract(year from rechazo.fecha)= extract(year from fecha_actual) and 
		extract(month from rechazo.fecha) = extract(month from fecha_actual) and 
		extract(day from rechazo.fecha) = extract(day from fecha_actual) and 
		rechazo.motivo='Supera el limite de tarjeta';
	
		select estado into estado_tarjeta from tarjeta t where t.nro_tarjeta = new.nro_tarjeta;

		if rechazo_registrado >= 2  and estado_tarjeta <> 'suspendida' then

			update tarjeta set estado = 'suspendida' where tarjeta.nro_tarjeta=new.nro_tarjeta; 
			insert into alerta values (pk_alerta,new.nro_tarjeta, new.fecha, new.nro_rechazo, 32,'limite de tarjeta excedido en el dia');

		end if;
		return new;

		end;
		$$ language plpgsql;



		create trigger alertaRechazosDiarios_trg
		after insert on rechazo
		for each row
		execute procedure alertaRechazosDiarios ();
		`)
	if err != nil {
		log.Fatal(err)
	}
}

//INSERTS

func agregarTarjetas(db *sql.DB, err error) {
	_, err = db.Exec(`
	insert into tarjeta values('1023202032800100',1,'202008','202408','8163',40000.10,'vigente');
	insert into tarjeta values('2138472174829390',2,'201902','202302','9281',35500.16,'vigente');
	insert into tarjeta values('5712238231843481',3,'201810','202210','1938',99900.05,'vigente');
	insert into tarjeta values('3248129374636441',4,'202007','202407','2039',65000.08,'vigente');
	insert into tarjeta values('3219821849123792',5,'201902','202302','9281',35500.16,'vigente');
	insert into tarjeta values('5385923614372932',6,'201509','203209','4871',50000.50,'vigente');
	insert into tarjeta values('6738451329865743',7,'201905','202305','9871',25000.10,'vigente'); 
	insert into tarjeta values('8274637103946203',8,'202001','202401','1876',20000.20,'vigente');
	insert into tarjeta values('5126734893240344',9,'201810','202210','4208',75000.10,'vigente');
	insert into tarjeta values('6562838203193614',10,'201911','202311','1984',30000.15,'vigente');
	insert into tarjeta values('3423456779756435',11,'201601','202001','1214',32200.15,'vencida'); 
	insert into tarjeta values('4326651759156405',11,'202002','202401','1215',30000.00,'vigente');
	insert into tarjeta values('4703299096980305',12,'202003','202403','4712',27000.85,'vigente');
	insert into tarjeta values('6011408239546626',13,'202009','202409','1763',30000.00,'anulada');
	insert into tarjeta values('3440546256710566',14,'201912','202312','3941',40500.00,'vigente'); 
	insert into tarjeta values('4030032322315337',15,'201409','202609','7722',55900.00,'suspendida');
	insert into tarjeta values('1023202032800007',16,'201007','202207','2567',25000.55,'vigente'); 
	insert into tarjeta values('2001445592130048',17,'201606','202806','5377',18000.50,'vigente');
	insert into tarjeta values('8976009822451268',18,'201711','202111','7213',60000.35,'vigente');
	insert into tarjeta values('3284542909990709',19,'201709','202109','2578',35000.90,'vigente');
	insert into tarjeta values('4454962801490329',19,'201509','201909','9775',45000.00,'vencida');
	insert into tarjeta values('5784542909990709',20,'202009','203209','5548',45000.90,'vigente');`)
	if err != nil {
		log.Fatal(err)
	}

}

func agregarCierres(db *sql.DB, err error) {
	_, err = db.Exec(`
	insert into cierre values(2021,1,0,'2020-12-22','2021-01-20','2021-02-01'); 
	insert into cierre values(2021,2,0,'2021-01-21','2021-02-22','2021-03-05'); 
	insert into cierre values(2021,3,0,'2021-02-23','2021-03-25','2021-04-02'); 
	insert into cierre values(2021,4,0,'2021-03-26','2021-04-25','2021-05-01');
	insert into cierre values(2021,5,0,'2021-04-26','2021-05-24','2021-06-05');
	insert into cierre values(2021,6,0,'2021-05-25','2021-06-22','2021-07-01');
	insert into cierre values(2021,7,0,'2021-06-23','2021-07-20','2021-08-03');
	insert into cierre values(2021,8,0,'2021-07-21','2021-08-23','2021-09-01');
	insert into cierre values(2021,9,0,'2021-08-24','2021-09-26','2021-10-04');
	insert into cierre values(2021,10,0,'2021-09-27','2021-10-26','2021-11-03');
	insert into cierre values(2021,11,0,'2021-10-27','2021-11-29','2021-12-08');
	insert into cierre values(2021,12,0,'2021-11-30','2021-12-31','2022-01-07');

	insert into cierre values(2021,1,1,'2021-01-01','2021-01-30','2021-02-05');
	insert into cierre values(2021,2,1,'2021-01-31','2021-02-28','2021-03-07');
	insert into cierre values(2021,3,1,'2021-03-01','2021-03-30','2021-04-06');
	insert into cierre values(2021,4,1,'2021-03-31','2021-04-30','2021-05-07');
	insert into cierre values(2021,5,1,'2021-05-01','2021-06-01','2021-06-08');
	insert into cierre values(2021,6,1,'2021-06-02','2021-07-02','2021-07-08');
	insert into cierre values(2021,7,1,'2021-07-03','2021-08-03','2021-08-07');
	insert into cierre values(2021,8,1,'2021-08-04','2021-09-01','2021-09-08');
	insert into cierre values(2021,9,1,'2021-09-02','2021-09-29','2021-10-03');
	insert into cierre values(2021,10,1,'2021-09-30','2021-10-26','2021-11-02');
	insert into cierre values(2021,11,1,'2021-10-27','2021-11-29','2021-12-07');
	insert into cierre values(2021,12,1,'2021-11-30','2021-12-31','2022-01-09');

	insert into cierre values(2021,1,2,'2020-01-01','2021-01-31','2021-02-06');
	insert into cierre values(2021,2,2,'2021-02-01','2021-03-01','2021-03-09');
	insert into cierre values(2021,3,2,'2021-03-02','2021-03-30','2021-04-05');
	insert into cierre values(2021,4,2,'2021-03-31','2021-04-28','2021-05-02');
	insert into cierre values(2021,5,2,'2021-04-29','2021-05-27','2021-06-04');
	insert into cierre values(2021,6,2,'2021-05-28','2021-06-28','2021-07-03');
	insert into cierre values(2021,7,2,'2021-06-29','2021-07-30','2021-08-08');
	insert into cierre values(2021,8,2,'2021-07-31','2021-08-27','2021-09-02');
	insert into cierre values(2021,9,2,'2021-08-28','2021-09-24','2021-10-01');
	insert into cierre values(2021,10,2,'2021-09-25','2021-10-26','2021-11-03');
	insert into cierre values(2021,11,2,'2021-10-27','2021-11-28','2021-12-06');
	insert into cierre values(2021,12,2,'2021-11-29','2021-12-29','2022-01-04');

	insert into cierre values(2021,1,3,'2021-01-01','2021-01-31','2021-02-05');
	insert into cierre values(2021,2,3,'2021-02-01','2021-02-28','2021-03-08');
	insert into cierre values(2021,3,3,'2021-03-01','2021-03-31','2021-04-04');
	insert into cierre values(2021,4,3,'2021-04-01','2021-04-29','2021-05-07');
	insert into cierre values(2021,5,3,'2021-04-30','2021-05-30','2021-06-05');
	insert into cierre values(2021,6,3,'2021-05-31','2021-06-30','2021-07-09');
	insert into cierre values(2021,7,3,'2021-07-01','2021-07-30','2021-08-06');
	insert into cierre values(2021,8,3,'2021-07-31','2021-08-28','2021-09-08');
	insert into cierre values(2021,9,3,'2021-08-29','2021-09-30','2021-10-06');
	insert into cierre values(2021,10,3,'2021-10-01','2021-10-31','2021-11-06');
	insert into cierre values(2021,11,3,'2021-11-01','2021-11-29','2021-12-08');
	insert into cierre values(2021,12,3,'2021-11-30','2021-12-31','2022-01-06');

	insert into cierre values(2021,1,4,'2021-01-01','2021-01-31','2021-02-04');
	insert into cierre values(2021,2,4,'2021-02-01','2021-02-28','2021-03-07');
	insert into cierre values(2021,3,4,'2021-03-01','2021-03-31','2021-04-06');
	insert into cierre values(2021,4,4,'2021-04-01','2021-04-29','2021-05-08');
	insert into cierre values(2021,5,4,'2021-04-30','2021-05-30','2021-06-04');
	insert into cierre values(2021,6,4,'2021-05-31','2021-06-30','2021-07-10');
	insert into cierre values(2021,7,4,'2021-07-01','2021-07-30','2021-08-07');
	insert into cierre values(2021,8,4,'2021-07-31','2021-08-28','2021-09-09');
	insert into cierre values(2021,9,4,'2021-08-29','2021-09-30','2021-10-07');
	insert into cierre values(2021,10,4,'2021-10-01','2021-10-31','2021-11-07');
	insert into cierre values(2021,11,4,'2021-11-01','2021-11-29','2021-12-08');
	insert into cierre values(2021,12,4,'2021-11-30','2021-12-31','2022-01-06');

	insert into cierre values(2021,1,5,'2021-01-01','2021-01-31','2021-02-06');
	insert into cierre values(2021,2,5,'2021-02-01','2021-02-28','2021-03-06');
	insert into cierre values(2021,3,5,'2021-03-01','2021-03-30','2021-04-06');
	insert into cierre values(2021,4,5,'2021-03-31','2021-04-30','2021-05-07');
	insert into cierre values(2021,5,5,'2021-05-01','2021-05-30','2021-06-07');
	insert into cierre values(2021,6,5,'2021-05-31','2021-06-29','2021-07-07');
	insert into cierre values(2021,7,5,'2021-06-30','2021-07-29','2021-08-06');
	insert into cierre values(2021,8,5,'2021-07-30','2021-08-28','2021-09-06');
	insert into cierre values(2021,9,5,'2021-08-29','2021-09-27','2021-10-08');
	insert into cierre values(2021,10,5,'2021-09-28','2021-10-30','2021-11-07');
	insert into cierre values(2021,11,5,'2021-10-31','2021-11-30','2021-12-08');
	insert into cierre values(2021,12,5,'2021-12-01','2021-12-31','2022-01-06');

	insert into cierre values(2021,1,6,'2021-01-01','2021-01-30','2021-02-06');
	insert into cierre values(2021,2,6,'2021-01-31','2021-02-28','2021-03-05');
	insert into cierre values(2021,3,6,'2021-03-01','2021-03-31','2021-04-07');
	insert into cierre values(2021,4,6,'2021-04-01','2021-04-30','2021-05-08');
	insert into cierre values(2021,5,6,'2021-05-01','2021-05-29','2021-06-07');
	insert into cierre values(2021,6,6,'2021-05-30','2021-06-29','2021-07-06');
	insert into cierre values(2021,7,6,'2021-06-30','2021-07-31','2021-08-06');
	insert into cierre values(2021,8,6,'2021-08-01','2021-08-31','2021-09-09');
	insert into cierre values(2021,9,6,'2021-09-01','2021-09-30','2021-10-07');
	insert into cierre values(2021,10,6,'2021-10-01','2021-10-31','2021-11-10');
	insert into cierre values(2021,11,6,'2021-11-01','2021-11-30','2021-12-2');
	insert into cierre values(2021,12,6,'2021-12-01','2021-12-31','2022-01-05');

	insert into cierre values(2021,1,7,'2021-01-01','2021-01-31','2021-02-08');
	insert into cierre values(2021,2,7,'2021-02-01','2021-02-28','2021-03-09');
	insert into cierre values(2021,3,7,'2021-03-01','2021-03-31','2021-04-03');
	insert into cierre values(2021,4,7,'2021-04-01','2021-04-30','2021-05-07');
	insert into cierre values(2021,5,7,'2021-05-01','2021-05-31','2021-06-10');
	insert into cierre values(2021,6,7,'2021-06-01','2021-06-30','2021-07-09');
	insert into cierre values(2021,7,7,'2021-07-01','2021-07-31','2021-08-06');
	insert into cierre values(2021,8,7,'2021-08-01','2021-08-31','2021-09-08');
	insert into cierre values(2021,9,7,'2021-09-01','2021-09-30','2021-10-07');
	insert into cierre values(2021,10,7,'2021-10-01','2021-10-31','2021-11-10');
	insert into cierre values(2021,11,7,'2021-11-01','2021-11-30','2021-12-04');
	insert into cierre values(2021,12,7,'2021-12-01','2021-12-31','2022-01-06');

	insert into cierre values(2021,1,8,'2021-01-01','2021-01-31','2021-02-08');
	insert into cierre values(2021,2,8,'2021-02-01','2021-02-28','2021-03-09');
	insert into cierre values(2021,3,8,'2021-03-01','2021-03-31','2021-04-06');
	insert into cierre values(2021,4,8,'2021-04-01','2021-04-30','2021-05-09');
	insert into cierre values(2021,5,8,'2021-05-01','2021-05-31','2021-06-12');
	insert into cierre values(2021,6,8,'2021-06-01','2021-06-30','2021-07-07');
	insert into cierre values(2021,7,8,'2021-07-01','2021-07-31','2021-08-06');
	insert into cierre values(2021,8,8,'2021-08-01','2021-08-31','2021-09-09');
	insert into cierre values(2021,9,8,'2021-09-01','2021-09-30','2021-10-07');
	insert into cierre values(2021,10,8,'2021-10-01','2021-10-31','2021-11-11');
	insert into cierre values(2021,11,8,'2021-11-01','2021-11-30','2021-12-05');
	insert into cierre values(2021,12,8,'2021-12-01','2021-12-31','2022-01-03');

	insert into cierre values(2021,1,9,'2021-01-01','2021-01-31','2021-02-08');
	insert into cierre values(2021,2,9,'2021-02-01','2021-02-28','2021-03-09');
	insert into cierre values(2021,3,9,'2021-03-01','2021-03-31','2021-04-03');
	insert into cierre values(2021,4,9,'2021-04-01','2021-04-30','2021-05-07');
	insert into cierre values(2021,5,9,'2021-05-01','2021-05-31','2021-06-11');
	insert into cierre values(2021,6,9,'2021-06-01','2021-06-30','2021-07-09');
	insert into cierre values(2021,7,9,'2021-07-01','2021-07-31','2021-08-06');
	insert into cierre values(2021,8,9,'2021-08-01','2021-08-31','2021-09-10');
	insert into cierre values(2021,9,9,'2021-09-01','2021-09-30','2021-10-05');
	insert into cierre values(2021,10,9,'2021-10-01','2021-10-31','2021-11-07');
	insert into cierre values(2021,11,9,'2021-11-01','2021-11-30','2021-12-04');
	insert into cierre values(2021,12,9,'2021-12-01','2021-12-31','2022-01-03');`)
	if err != nil {
		log.Fatal(err)
	}

}

func agregarComercios(db *sql.DB, err error) {
	_, err = db.Exec(`
	insert into comercio values(1,'Starbucks','Arturo Umberto Illia 3770','B1613HBR','080012201291');
	insert into comercio values(2,'Shell','Raul Sacalabrini Ortiz','C1414DNG','011483176032');
	insert into comercio values(3,'Dexter','Av. Pres. Juan Domingo Perón 1550','B1663GHQ','541120912972');
	insert into comercio values(4,'Burger King','Av Corrientes 4675','C1195AAF','541123652489');
	insert into comercio values(5,'Grido','La Rioja 2102','C1214ADB','011270770586');
	insert into comercio values(6,'Ferreteria Lavalle','Gral. Lavalle 8348','B1657DAF','541147393750');
	insert into comercio values(7,'Iguana PC','Santos Vega 6006','B1682AIH','541569473274');
	insert into comercio values(8,'Textil Hogar Villa Bosch','Gaucho Cruz 5481','B1682AKI','541148446790');
	insert into comercio values(9,'Hotel Burbujas','Santos Vega 6357','B1657AJF','541156336881');
	insert into comercio values(10,'Relojeria 1 De Mayo','1 de Mayo 1022','B1657BRH','541148480090');
	insert into comercio values(11,'Mi Gusto','Serrano 1665','B1663GUK','541154664200');
	insert into comercio values(12,'El Insumo SRL','Tucumán 2086','C1050AAP','541143723132');
	insert into comercio values(13,'InterLiving','Av. Gral. Miguel de Azcuenaga 661','B1708ESI','541144200448');
	insert into comercio values(14,'Lo de Carlitos','Coronel Charlone 1088','B1663NTU','541156672901');
	insert into comercio values(15,'Las Margaritas','Av. Dr. Ricardo Balbín 1399','B1663NDA','541170868526');
	insert into comercio values(16,'Mc Donalds','Av. Pres. Juan Domingo Perón 1522','B1663GHQ','1144551828');
	insert into comercio values(17,'Nosh Up','Entre Ríos 698','B1661BUO','1120045123');
	insert into comercio values(18,'Coto','Av. Pres. Hipólito Yrigoyen 1826' ,'B1665ABS','02320428689');
	insert into comercio values(19,'Sarkany','Paunero 1479','B1663GJG','1146673212');
	insert into comercio values(20,'Heladería Cremolatti','Av. Pres. Juan Domingo Perón 665','B1662ASG','1153505307');`)
	if err != nil {
		log.Fatal(err)
	}
}

func agregarClientes(db *sql.DB, err error) {
	_, err = db.Exec(`
	insert into cliente values(1,'Ivan','Martinez','Ramos Mejia 1256',01145232718);
	insert into cliente values(2,'Lucas','Ramires','4 de Marzo 568',01156485214);
	insert into cliente values(3,'Julieta','Diaz','Mario Bravo 5682',01145781262);
	insert into cliente values(4,'Ignacio','Ferreira','Paracas 564',011358796543);
	insert into cliente values(5,'Valentina','Sanchez','Santos Vega 856',01178452326);
	insert into cliente values(6,'Maria','Blampied','Santa Rita 8760',01126541242);
	insert into cliente values(7,'Horacio','Franzetti','Machado 8867', 01129234922);
	insert into cliente values(8,'Julieta','Grimaldi','Santos Dumont 3743',01164512897);
	insert into cliente values(9,'Gabriela','Brunalti','Libertad 310',01185412349);
	insert into cliente values(10,'Martin','Latorre','Mansilla 1133',01149231594);
	insert into cliente values(11,'Matias','Lopez','Alberdi 2450',01155931630);
	insert into cliente values(12,'Gimena','Perez','Juan B Justo 1634',01123253467);
	insert into cliente values(13,'Belen','Cristaldo','Lavalle 2206',01134546587);
	insert into cliente values(14,'Natacha','Ojeda','Avenida Presidente Perón 4058',01130175637);
	insert into cliente values(15,'Lautaro','Flores','Martín Irigoyen 980',01150328339);
	insert into cliente values(16,'Fabiana','Martinez','San José 5586',01132842271);
	insert into cliente values(17,'Maria','Fernandez','España 4484',01154279063);
	insert into cliente values(18,'Rodrigo','Peralta','Constitución 1861',01164513527);
	insert into cliente values(19,'Roberto','Valenzuela','Saavedra 1292',01128439568);
	insert into cliente values(20,'Agustina','Pereyra','Tres Arroyos 3836',01169869423);`)
	if err != nil {
		log.Fatal(err)
	}
}

func agregarConsumos(db *sql.DB, err error) {
	_, err = db.Exec(`
	
	insert into consumo values('3248129374636441','2039',5,100.0);-- consumos en menos de 5 minutos distinto cod postal
	insert into consumo values('3248129374636441','2039',10,10.0);
	
	insert into consumo values('1023202032800100','8163',3,1000.0);  -- consumos en menos de un minuto mismo cod postal
	insert into consumo values('1023202032800100','8163',16,30000.0);
	
	insert into consumo values('5385923614372932','4871',10,60000.0); -- dos consumos con excesos de limite en el mismo dia
	insert into consumo values('5385923614372932','4871',11,70000.0);
	insert into consumo values('5385923614372932','4871',12,80000.0);

	-- todos estos testean el triggerRechazo(se agregan a la tabla alerta automaticamente cuando entran el rechazo)
	insert into consumo values('8274637103946203','1000',15,100.0); -- codigo invalido
	
	insert into consumo values('4521563258974582','1265',18,200.0); -- tarjeta inexistente
	insert into consumo values('6011408239546626','1763',15,500.0); -- tarjeta anulada
	
	insert into consumo values('4030032322315337','7722',2,800.0); -- tarjeta suspendida
	
	insert into consumo values('2001445592130048','5377',5,51000.0); -- supera limite
	
	insert into consumo values('3423456779756435','1214',11,50.0); -- tarjeta vencida
	
	`)
	if err != nil {
		log.Fatal(err)
	}
}

//OTROS

func mostrarConsumos(consumos []consumo) {
	for _, v := range consumos {
		fmt.Printf("-----------------------------------------------\n")
		fmt.Printf("Nro tarjeta: %v\nCod Seguridad: %v\nNro Comercio: %v\nMonto: %v\n", v.NroTarjeta, v.Cod, v.NroComercio, v.Monto)
		fmt.Printf("-----------------------------------------------\n")
	}
}

func eliminarPksFks(db *sql.DB, err error) {
	_, err = db.Exec(`select eliminarPKs();`)
	if err != nil {
		log.Fatal(err)
	}
}

func autorizacionDeCompra(db *sql.DB, err error, nroTarjeta string, cod string, numero string, monto string) bool {
	ret := false
	rows, err := db.Query(`select autorizacionDeCompra('` + nroTarjeta + `','` + cod + `',` + numero + `,` + monto + `);`)
	defer rows.Close()
	for rows.Next() {
		if err := rows.Scan(&ret); err != nil {
			log.Fatal(err)
		}
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	return ret
}

func generacionDeResumen(db *sql.DB, err error, nroTarjeta string, periodo string) {
	_, err = db.Exec(`select generarResumen('` + nroTarjeta + `',` + periodo + `);`)
	if err != nil {
		log.Fatal(err)
	}
}

func obtenerConsumos(db *sql.DB, err error) []consumo {
	var c consumo
	var consumos []consumo
	rows, err := db.Query(`select * from consumo;`)
	defer rows.Close()
	for rows.Next() {
		if err := rows.Scan(&c.NroTarjeta, &c.Cod, &c.NroComercio, &c.Monto); err != nil {
			log.Fatal(err)
		}
		consumos = append(consumos, c)
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	return consumos
}

func agregarFunciones(db *sql.DB, err error) {
	crearAutorizacionDeCompra(db, err)
	crearTarjetaNoValida(db, err)
	crearCodInvalido(db, err)
	crearMontoNoPermitido(db, err)
	crearTarjetaSuspendida(db, err)
	crearGenerarResumen(db, err)
	crearTarjetaVencida(db, err)
	crearEliminarPKs(db, err)
}

func agregarTriggers(db *sql.DB, err error) {
	crearTriggerRechazo(db, err)
	crearTriggerComprasEnCincoMinutos(db, err)
	crearTriggerComprasEnUnMinuto(db, err)
	agregarTriggerRechazosDiarios(db, err)
}

func main() {

	createDatabase()

	scanner := bufio.NewScanner(os.Stdin)
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetasdecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var (
		tablasOK    = true
		pksFksOK    = true
		funcionesOK = true
		triggersOK  = true
		cierresOK   = true
		comerciosOK = true
		tarjetasOK  = true
		clientesOK  = true
		consumosOK  = true
		terminado   = false
	)

	//El paquete bufio implementa E / S con búfer.
	//os lee la entrada estándar como un conjunto de líneas.
	fmt.Printf("Bienvenido \nIngrese una opción\n")
	fmt.Printf("Presione 1 para crear las tablas\n")
	fmt.Printf("Presione 2 para crear fks y pks\n")
	fmt.Printf("Presione 3 para crear clientes\n")
	fmt.Printf("Presione 4 para crear los cierres\n")
	fmt.Printf("Presione 5 para crear tarjetas\n")
	fmt.Printf("Presione 6 para crear comercios\n")
	fmt.Printf("Presione 7 para crear consumos\n")
	fmt.Printf("Presione 8 para crear las funciones\n")
	fmt.Printf("Presione 9 para crear los triggers\n")
	//Llenar base de datos
	for scanner.Scan() && !terminado {
		input := scanner.Text()
		if input == "1" && tablasOK {
			crearTablas(db, err)
			tablasOK = false
			fmt.Printf("Tablas creadas\n")
		}
		if input == "2" && pksFksOK {
			crearPksYFks(db, err)
			pksFksOK = false
			fmt.Printf("PKS Y FKS creadas\n")
		}
		if input == "3" && clientesOK && !tablasOK {
			agregarClientes(db, err)
			clientesOK = false
			fmt.Printf("Clientes creados\n")
		}
		if input == "4" && cierresOK && !tablasOK {
			agregarCierres(db, err)
			cierresOK = false
			fmt.Print("Cierres creados\n")
		}
		if input == "5" && tarjetasOK && !tablasOK {
			agregarTarjetas(db, err)
			tarjetasOK = false
			fmt.Printf("Tarjetas creadas\n")
		}
		if input == "6" && comerciosOK && !tablasOK {
			agregarComercios(db, err)
			comerciosOK = false
			fmt.Print("Comercios creados\n")
		}
		if input == "7" && consumosOK && !tablasOK {
			agregarConsumos(db, err)
			consumosOK = false
			fmt.Printf("Consumos creados\n")
		}
		if input == "8" && funcionesOK && !tablasOK {
			agregarFunciones(db, err)
			funcionesOK = false
			fmt.Printf("Funciones creadas\n")
		}
		if input == "9" && triggersOK && !tablasOK {
			agregarTriggers(db, err)
			triggersOK = false
			fmt.Print("Triggers agregados\n")
		}
		if !tablasOK && !pksFksOK && !funcionesOK && !triggersOK && !cierresOK && !comerciosOK && !consumosOK && !clientesOK && !tarjetasOK {
			fmt.Print("Base de datos de datos lista para su uso\n")
			terminado = true
		}
	}

	fmt.Print("-----------------------------------------------------\n")

	fmt.Print("Presione 1 hacer los consumos al sistema\n")
	fmt.Print("Presione 2 para generar resumen de los consumos\n")
	fmt.Print("Presione 3 para eliminar las pks\n")
	fmt.Print("Presione 4 para mostrar los consumos\n")
	fmt.Print("Presione 5 para salir\n")

	terminado = false
	for scanner.Scan() && !terminado {
		input := scanner.Text()

		if input == "1" {
			consumos := obtenerConsumos(db, err)
			for _, v := range consumos {
				ok := autorizacionDeCompra(db, err, v.NroTarjeta, v.Cod, v.NroComercio, v.Monto)
				if ok {
					fmt.Printf("Compra autorizada\n")
				} else {
					fmt.Printf("Compra Denegada\n")
				}
			}
		}
		if input == "2" {
			consumos := obtenerConsumos(db, err)
			for _, v := range consumos {
				generacionDeResumen(db, err, v.NroTarjeta, "6")
			}
			fmt.Printf("resumenes cargados en la base de datos\n")
		}
		if input == "3" {
			eliminarPksFks(db, err)
			fmt.Printf("Fks y PKs borradas\n")
		}
		if input == "4" {
			mostrarConsumos(obtenerConsumos(db, err))
		}
		if input == "5" {
			fmt.Printf("Adios!\n")
			terminado = true
		}
	}
}
