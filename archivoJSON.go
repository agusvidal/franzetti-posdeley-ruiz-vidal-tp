package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	bolt "github.com/coreos/bbolt"
	_ "github.com/lib/pq"
	"log"
	"os"
	"strconv"
)

type Cliente struct {
	Nro_cliente int
	Nombre      string
	Apellido    string
	Domicilio   string
	Telefono    string
}

type Tarjeta struct {
	Nro_tarjeta   int
	Nro_cliente   int
	Valida_desde  string
	Valida_hasta  string
	Cod_seguridad string
	Limite_compra float64
	Estado        string
}

type Comercio struct {
	Nro_comercio  int
	Nombre        string
	Domicilio     string
	Codigo_postal string
	Telefono      string
}

type Compra struct {
	Nro_operacion int
	Nro_tarjeta   string
	Nro_comercio  int
	Fecha         string
	Monto         float64
	Pagado        bool
}

//crea el backet si no existe - asocia en la base de datos una key con un valor
func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transacción de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName)) //crea el bucket e ignora el codigo de error

	err = b.Put(key, val) //asocia la key con el valor
	if err != nil {
		return err
	}

	// cierra transacción
	if err := tx.Commit(); err != nil { //guarda cambios BD nosql
		return err
	}

	return nil
}

//busca buvket - devuelve el valor asociado a la key
func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	// abre una transacción de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}

func bienvenido() {

	fmt.Printf("")
	fmt.Printf("")
	fmt.Printf("")
	fmt.Printf("")
	fmt.Printf("")
	fmt.Printf("Ingrese una opción\n")
	fmt.Printf("Presione 1 para cargar Clientes\n")
	fmt.Printf("Presione 2 para cargar Tarjetas\n")
	fmt.Printf("Presione 3 para cargar Comercios\n")
	fmt.Printf("Presione 4 para cargar Compras\n")
	fmt.Printf("Presione 5 para cargar finalizar\n")
}

func main() {
	var terminado = false
	scanner := bufio.NewScanner(os.Stdin)
	db, err := bolt.Open("tarjetasDeCredito.db", 0600, nil) //0600 permisos archivo escritura y lectura
	if err != nil {
		log.Fatal(err)
	}

	bienvenido()

	//Llenar base de datos
	for scanner.Scan() && !terminado {
		input := scanner.Text()
		if input == "1" {

			clientes := []Cliente{
				{Nro_cliente: 1, Nombre: "Ivan", Apellido: "Martinez", Domicilio: "Ramos Mejia 1256", Telefono: "01145232718"},
				{Nro_cliente: 2, Nombre: "Lucas", Apellido: "Ramires", Domicilio: "4 de Marzo 568", Telefono: "01156485214"},
				{Nro_cliente: 3, Nombre: "Julieta'", Apellido: "Diaz", Domicilio: "Mario Bravo 5682", Telefono: "01145781262"},
			}

			var i = 0

			for i < 3 {
				data_clientes, err := json.MarshalIndent(clientes[i], "", "    ")

				if err != nil {
					log.Fatalf("JSON marshaling failed: %s", err)
				}

				CreateUpdate(db, "clientes", []byte(strconv.Itoa(clientes[i].Nro_cliente)), data_clientes)
				resultado, err := ReadUnique(db, "clientes", []byte(strconv.Itoa(clientes[i].Nro_cliente)))
				fmt.Printf("%s\n", resultado)
				i++
			}

			bienvenido()
		}

		if input == "2" {

			tarjetas := []Tarjeta{
				{Nro_tarjeta: 1023202032800100, Nro_cliente: 1, Valida_desde: "202008", Valida_hasta: "202408", Cod_seguridad: "8163", Limite_compra: 4000010.45, Estado: "vigente"},
				{Nro_tarjeta: 2138472174829390, Nro_cliente: 2, Valida_desde: "201902", Valida_hasta: "202302", Cod_seguridad: "9281", Limite_compra: 3550016.34, Estado: "vigente"},
				{Nro_tarjeta: 5712238231843481, Nro_cliente: 3, Valida_desde: "201810", Valida_hasta: "202210", Cod_seguridad: "1938", Limite_compra: 9990005.28, Estado: "vigente"},
			}

			var j = 0

			for j < 3 {
				data_tarjetas, err := json.MarshalIndent(tarjetas[j], "", "    ")

				if err != nil {
					log.Fatalf("JSON marshaling failed: %s", err)
				}

				CreateUpdate(db, "tarjetas", []byte(strconv.Itoa(tarjetas[j].Nro_tarjeta)), data_tarjetas)
				resultado2, err := ReadUnique(db, "tarjetas", []byte(strconv.Itoa(tarjetas[j].Nro_tarjeta)))
				fmt.Printf("%s\n", resultado2)
				j++
			}

			bienvenido()
		}

		if input == "3" {

			comercios := []Comercio{
				{Nro_comercio: 1, Nombre: "Starbucks", Domicilio: "Arturo Umberto Illia 3770", Codigo_postal: "B1613HBR", Telefono: "080012201291"},
				{Nro_comercio: 2, Nombre: "Shell", Domicilio: "Raul Sacalabrini Ortiz", Codigo_postal: "C1414DNG", Telefono: "011483176032"},
				{Nro_comercio: 3, Nombre: "Dexter", Domicilio: "Av. Pres. Juan Domingo Perón 1550", Codigo_postal: "B1663GHQ", Telefono: "541120912972"},
			}

			var k = 0

			for k < 3 {
				data_comercios, err := json.MarshalIndent(comercios[k], "", "    ")

				if err != nil {
					log.Fatalf("JSON marshaling failed: %s", err)
				}

				CreateUpdate(db, "comercios", []byte(strconv.Itoa(comercios[k].Nro_comercio)), data_comercios)
				resultado3, err := ReadUnique(db, "comercios", []byte(strconv.Itoa(comercios[k].Nro_comercio)))
				fmt.Printf("%s\n", resultado3)
				k++
			}

			bienvenido()
		}

		if input == "4" {

			compras := []Compra{
				{Nro_operacion: 1, Nro_tarjeta: "1023202032800100", Nro_comercio: 3, Fecha: "2021-05-01", Monto: 1000.55, Pagado: true},
				{Nro_operacion: 2, Nro_tarjeta: "5126734893240344", Nro_comercio: 3, Fecha: "2021-07-04", Monto: 900.45, Pagado: false},
				{Nro_operacion: 3, Nro_tarjeta: "8976009822451269", Nro_comercio: 20, Fecha: "2021-09-09", Monto: 1500.90, Pagado: true},
			}

			var n = 0

			for n < 3 {
				data_compras, err := json.MarshalIndent(compras[n], "", "    ")

				if err != nil {
					log.Fatalf("JSON marshaling failed: %s", err)
				}

				CreateUpdate(db, "compras", []byte(strconv.Itoa(compras[n].Nro_operacion)), data_compras)
				resultado4, err := ReadUnique(db, "compras", []byte(strconv.Itoa(compras[n].Nro_operacion)))
				fmt.Printf("%s\n", resultado4)
				n++
			}

			bienvenido()
		}

		if input == "5" {

			terminado = true
			fmt.Printf("Adios!\n")
		}

	}
}
