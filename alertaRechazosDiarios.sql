
create or replace function alertaRechazosDiarios() returns trigger as $$

declare
rechazo_registrado int;
pk_alerta int;
fecha_actual timestamp;
estado_tarjeta varchar(15);
begin

select count(nro_alerta) into pk_alerta from alerta;
pk_alerta := pk_alerta + 1;
fecha_actual := new.fecha;
	
select count(*) into rechazo_registrado from rechazo where rechazo.nro_tarjeta = new.nro_tarjeta and 
extract(year from rechazo.fecha)= extract(year from fecha_actual) and 
extract(month from rechazo.fecha) = extract(month from fecha_actual) and 
extract(day from rechazo.fecha) = extract(day from fecha_actual) and 
rechazo.motivo='Supera el limite de tarjeta';

select estado into estado_tarjeta from tarjeta t where t.nro_tarjeta = new.nro_tarjeta;

if rechazo_registrado >= 2 and estado_tarjeta <> 'suspendida' then

	update tarjeta set estado = 'suspendida' where tarjeta.nro_tarjeta=new.nro_tarjeta; 
	insert into alerta values (pk_alerta,new.nro_tarjeta, new.fecha, new.nro_rechazo, 32,'limite de tarjeta excedido en el dia');

end if;
return new;

end;
$$ language plpgsql;



create trigger alertaRechazosDiarios_trg
after insert on rechazo
for each row
execute procedure alertaRechazosDiarios ();

