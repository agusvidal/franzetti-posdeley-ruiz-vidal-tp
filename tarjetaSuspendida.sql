create or replace function tarjetaSuspendida(nro_tarj varchar(16)) returns boolean as $$
declare                                                                 
                                                                        
resultado record;                                                       
                                                                        
begin                                                                   
                                                                        
select * into resultado from tarjeta where tarjeta.nro_tarjeta=nro_tarj;          
                                                                        
if found then                                                           
        if resultado.estado = 'suspendida' then                         
                return true;                                            
                                                                        
            end if;                                                     
end if;                                                                 
                                                                        
                                                                        
return false;                                                           
                                                                        
end;                                                                    
$$ language plpgsql;   
