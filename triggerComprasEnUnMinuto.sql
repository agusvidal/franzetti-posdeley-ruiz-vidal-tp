create or replace function triggerComprasEnUnMinuto() returns trigger as $$       
                                                                        
declare                                                                 
   
	fecha_actual timestamp;
    compras	 int;
	pk_alerta int;
	
                                                                        
begin   

	select count(nro_alerta) into pk_alerta from alerta;                  
    pk_alerta := pk_alerta + 1;

	fecha_actual := new.fecha;
	
	select count(*) into compras from comercio, compra
	where compra.nro_comercio = comercio.nro_comercio and 
	extract(year from fecha_actual) = extract(year from compra.fecha) and extract(month from fecha_actual) = extract(month from compra.fecha) and 
	extract(day from fecha_actual)= extract(day from compra.fecha) and 
	new.nro_tarjeta = compra.nro_tarjeta and 
	((extract (hour from fecha_actual)*3600 + extract (minute from fecha_actual)*60+ extract(second from fecha_actual))-
	(extract (hour from fecha)*3600+ extract(minute from fecha)*60 + extract(second from fecha)))<61 and
	compra.nro_tarjeta=new.nro_tarjeta and 
	compra.nro_comercio <> new.nro_comercio and
	(select codigo_postal from comercio c where c.nro_comercio = new.nro_comercio) = (select codigo_postal from comercio c where c.nro_comercio = compra.nro_comercio);

	if compras>0 then
		insert into alerta values(pk_alerta, new.nro_tarjeta, new.fecha, null, 1, 'se hizo mas de una compra en un minuto'); 
	end if;
		return new;                                                         
                                                                        
end;                                                                    
$$ language plpgsql;                                                    
                                                                        
create trigger comprasProximasAUnMin_trg                                              
after insert on compra                                                
for each row                                                            
execute procedure triggerComprasEnUnMinuto();
