create or replace function generarResumen(nro_tarj varchar(16),periodo int) returns void as $$
declare
	pk_resumen int;
	pk_linea int;
	cliente record;
	tarjeta record;
	compras record;
	cierre record;
	ultimo_digito char;
	total_resumen decimal(8,2);
	nombre_comercio text;
                                                                        
begin

	select count(nro_resumen) into pk_resumen from cabecera;
    pk_resumen := pk_resumen + 1;


	select * into cliente from cliente where nro_cliente in (
		select nro_cliente from tarjeta where nro_tarjeta = nro_tarj); -- encuentro el cliente

	select * into tarjeta from tarjeta where nro_tarjeta = nro_tarj;  -- encuentro la tarjeta

	select into ultimo_digito right(tarjeta.nro_tarjeta,1);   -- guardo la terminacion de la tarjeta

	select * into cierre from cierre c where c.mes = periodo and c.terminacion = (select(ultimo_digito :: int)); -- encuentro cierre

	pk_linea := 0;
	total_resumen := 0;
	
	if cierre.fecha_inicio not in (select desde from cabecera) and cierre.fecha_cierre not in (select hasta from cabecera) then

		for compras in select * from compra loop
		
			if compras.nro_tarjeta = tarjeta.nro_tarjeta and cierre.fecha_inicio<=compras.fecha and cierre.fecha_cierre>=compras.fecha 
			   and compras.pagado = false then

				select into nombre_comercio from comercio c where c.nro_comercio = compras.nro_comercio; 
				
				insert into detalle values(pk_resumen,
										   pk_linea,
										   compras.fecha,
										   nombre_comercio,
										   compras.monto);

				total_resumen := total_resumen + compras.monto;
				
				pk_linea := pk_linea + 1;

			end if;
		
		end loop;
		
		
		
		insert into cabecera values(pk_resumen,
									cliente.nombre,
									cliente.apellido,
									cliente.domicilio,
									nro_tarj,
									cierre.fecha_inicio,
									cierre.fecha_cierre,
									cierre.fecha_vto,
									total_resumen);
	end if;
                                                                        
end;                                                                    
$$ language plpgsql;      
