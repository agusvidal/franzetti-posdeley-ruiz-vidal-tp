create or replace function codInvalido(nro_tarj varchar(16),cod varchar(4)) returns boolean as $$ 
declare

resultado record;

begin

select * into resultado from tarjeta where nro_tarjeta=nro_tarj;
if found then
	if resultado.cod_seguridad<>cod then
		return true;
	end if;
end if;

return false;

end;
$$ language plpgsql;
