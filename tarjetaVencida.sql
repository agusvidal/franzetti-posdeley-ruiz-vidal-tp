create or replace function tarjetaVencida(nro_tarj varchar(16))returns boolean as $$ 
declare
resultado record;                                                       
fecha timestamp;                                                        
mes_vencimiento varchar(2);                                              
anio_vencimiento varchar(4);     
anio_actual varchar(4);
mes_actual varchar(2);                                        
                                                                        
begin                                                                   
    select * into resultado from tarjeta where nro_tarjeta=nro_tarj;      
                                                                        
    if found then                                                       
        fecha:=current_timestamp;                                       
        select extract(year from fecha) into anio_actual;  
        select extract(month from fecha) into mes_actual;                 
        anio_vencimiento:=substring(resultado.valida_hasta from 1 for 4); 
        mes_vencimiento:= substring(resultado.valida_hasta from 5 for 6);    
        if anio_actual=anio_vencimiento and mes_actual>=mes_vencimiento then
            return true;                                                
        end if;                                                         
        if anio_actual>anio_vencimiento then                              
            return true;                                                
        end if;                                                         
end if;                                                                 
return false; 
end;                                                                    
$$ language plpgsql;                                                    
  
