create or replace function tarjetaNoValida(nro varchar(16)) returns boolean as $$
declare                                                                 
                                                                        
resultado record;                                                       
                                                                                                                                         
begin                                                                   
                                                                        
select * into resultado from tarjeta where nro_tarjeta = nro;            
if found then                                                           
    if resultado.estado = 'vigente' then                                
        return false;                                                    
    end if;                                                             
end if;                                                                 
                                                                        
return true;                                                           
                                                                        
end;                                                                    
$$ language plpgsql;  
