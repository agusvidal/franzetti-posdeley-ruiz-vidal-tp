drop database if exists basededatos;
create database basededatos;
\c basededatos

create table cliente(
	nro_cliente  int,
	nombre  text,
	apellido text,
	domicilio text,
	telefono char(12)
);

create table tarjeta(
	nro_tarjeta  char(16), 
	nro_cliente        int,
	valida_desde       char(6),
	valida_hasta	char(6),
	cod_seguridad	char(4),
	limite_compra	decimal(8,2),
	estado   char(10)
);

create table comercio(
	nro_comercio   int,
	nombre		text, 
	domicilio	text,
	codigo_postal	char(8),
	telefono	char(12)
);

create table compra(
	nro_operacion 	int,
	nro_tarjeta	char(16),
	nro_comercio	int,
	fecha	timestamp,
	monto	decimal(7,2),
	pagado 	boolean
);

create table rechazo(
	nro_rechazo 	int,
	nro_tarjeta	char(16),
	nro_comercio	int,
	fecha	timestamp,
	monto	decimal(7,2),
	motivo 	text
);

create table cierre(
	anio 	int,
	mes	int,
	terminacion	int,
	fecha_inicio	date,
	fecha_cierre	date,
	fecha_vto	date
);

create table cabecera(
	nro_resumen 	int,
	nombre	text,
	apellido	text,
	domicilio	text,
	nro_tarjeta	char(16),
	desde	date,
	hasta	date,
	vence	date,
	total	decimal(8,2)
);

create table detalle(
	nro_resumen 	int,
	nro_linea	int,
	fecha	date,
	nombre_comercio	text,
	monto	decimal(7,2)
);

create table alerta(	
	nro_alerta	int,
	nro_tarjeta	char(16),
	fecha	timestamp,
	nro_rechazo	int,
	cod_alerta	int,	--0:rechazo, 1:compra 1min, 5:compra 5min, 32:límite
	descripcion	text
);

-- Esta tabla *no* es parte del modelo de datos, pero se incluye para
-- poder probar las funciones.
create table consumo(
	nro_tarjeta	char(16),
	cod_seguridad	char(4),
	nro_comercio	int,
	monto	decimal(7,2)
);

-- PKS
alter table cliente add constraint cliente_pk  primary key (nro_cliente);

alter table tarjeta add constraint tarjeta_pk primary key (nro_tarjeta);

alter table comercio add constraint comercio_pk primary key (nro_comercio);

alter table compra add constraint compra_pk primary key (nro_operacion);

alter table rechazo add constraint rechazo_pk primary key (nro_rechazo);

alter table cierre add constraint cierre_pk primary key (anio,mes,terminacion);

alter table cabecera add constraint cabecera_pk primary key (nro_resumen);

alter table detalle add constraint detalle_pk primary key (nro_resumen,nro_linea);

alter table alerta add constraint alerta_pk primary key (nro_alerta);

-- FKS
alter table tarjeta add constraint tarjeta_nro_cliente_fk foreign key (nro_cliente) references cliente (nro_cliente);

alter table compra add constraint compra_nro_tarjeta_fk foreign key (nro_tarjeta) references tarjeta (nro_tarjeta);

alter table compra add constraint compra_nro_comercio_fk foreign key (nro_comercio) references comercio (nro_comercio);

alter table cabecera add constraint cabecera_nro_tarjeta_fk foreign key (nro_tarjeta) references tarjeta (nro_tarjeta);


alter table consumo add constraint consumo_nro_comercio_fk foreign key (nro_comercio) references comercio (nro_comercio);

--Personas:


insert into cliente values(1,'Ivan','Martinez','Ramos Mejia 1256',01145232718);
insert into cliente values(2,'Lucas','Ramires','4 de Marzo 568',01156485214);
insert into cliente values(3,'Julieta','Diaz','Mario Bravo 5682',01145781262);
insert into cliente values(4,'Ignacio','Ferreira','Paracas 564',011358796543);
insert into cliente values(5,'Valentina','Sanchez','Santos Vega 856',01178452326);
insert into cliente values(6,'Maria','Blampied','Santa Rita 8760',01126541242);
insert into cliente values(7,'Horacio','Franzetti','Machado 8867', 01129234922);
insert into cliente values(8,'Julieta','Grimaldi','Santos Dumont 3743',01164512897);
insert into cliente values(9,'Gabriela','Brunalti','Libertad 310',01185412349);
insert into cliente values(10,'Martin','Latorre','Mansilla 1133',01149231594);
insert into cliente values(11,'Matias','Lopez','Alberdi 2450',01155931630);
insert into cliente values(12,'Gimena','Perez','Juan B Justo 1634',01123253467);
insert into cliente values(13,'Belen','Cristaldo','Lavalle 2206',01134546587);
insert into cliente values(14,'Natacha','Ojeda','Avenida Presidente Perón 4058',01130175637);
insert into cliente values(15,'Lautaro','Flores','Martín Irigoyen 980',01150328339);
insert into cliente values(16,'Fabiana','Martinez','San José 5586',01132842271);
insert into cliente values(17,'Maria','Fernandez','España 4484',01154279063);
insert into cliente values(18,'Rodrigo','Peralta','Constitución 1861',01164513527);
insert into cliente values(19,'Roberto','Valenzuela','Saavedra 1292',01128439568);
insert into cliente values(20,'Agustina','Pereyra','Tres Arroyos 3836',01169869423);


--Tarjetas: 

insert into tarjeta values('1023202032800100',1,'202008','202408','8163',40000.10,'vigente');-- para testear compras en un minuto
insert into tarjeta values('2138472174829390',2,'201902','202302','9281',35500.16,'vigente');
insert into tarjeta values('5712238231843481',3,'201810','202210','1938',99900.05,'vigente');
insert into tarjeta values('3248129374636441',4,'202007','202407','2039',65000.08,'vigente');-- para testear compras en 5 minutos
insert into tarjeta values('3219821849123792',5,'201902','202302','9281',35500.16,'vigente');
insert into tarjeta values('5385923614372932',6,'201509','203209','4871',50000.50,'vigente'); -- para testear dos excesos de limite en el mismo dia
insert into tarjeta values('6738451329865743',7,'201905','202305','9871',25000.10,'vigente'); 
insert into tarjeta values('8274637103946203',8,'202001','202401','1876',20000.20,'vigente'); -- para testear cod seguridad
insert into tarjeta values('5126734893240344',9,'201810','202210','4208',75000.10,'vigente');
insert into tarjeta values('6562838203193614',10,'201911','202311','1984',30000.15,'vigente');
insert into tarjeta values('3423456779756435',11,'201601','202001','1214',32200.15,'vencida'); --para testear si esta vencida
insert into tarjeta values('4326651759156405',11,'202002','202401','1215',30000.00,'vigente');
insert into tarjeta values('4703299096980305',12,'202003','202403','4712',27000.85,'vigente');
insert into tarjeta values('6011408239546626',13,'202009','202409','1763',30000.00,'anulada');--para testear TarjetaVigente
insert into tarjeta values('3440546256710566',14,'201912','202312','3941',40500.00,'vigente'); 
insert into tarjeta values('4030032322315337',15,'201409','202609','7722',55900.00,'suspendida'); --para testear tarjeta suspendida
insert into tarjeta values('1023202032800007',16,'201007','202207','2567',25000.55,'vigente'); 
insert into tarjeta values('2001445592130048',17,'201606','202806','5377',18000.50,'vigente'); --para testear monto permitido
insert into tarjeta values('8976009822451268',18,'201711','202111','7213',60000.35,'vigente');
insert into tarjeta values('3284542909990709',19,'201709','202109','2578',35000.90,'vigente');
insert into tarjeta values('4454962801490329',19,'201509','201909','9775',45000.00,'vencida');
insert into tarjeta values('5784542909990709',20,'202009','203209','5548',45000.90,'vigente');


--Comercios:


insert into comercio values(1,'Starbucks','Arturo Umberto Illia 3770','B1613HBR','080012201291');
insert into comercio values(2,'Shell','Raul Sacalabrini Ortiz','C1414DNG','011483176032');
insert into comercio values(3,'Dexter','Av. Pres. Juan Domingo Perón 1550','B1663GHQ','541120912972');--Sirve para testear funcion de compras por minuto 
insert into comercio values(4,'Burger King','Av Corrientes 4675','C1195AAF','541123652489');
insert into comercio values(5,'Grido','La Rioja 2102','C1214ADB','011270770586');
insert into comercio values(6,'Ferreteria Lavalle','Gral. Lavalle 8348','B1657DAF','541147393750');
insert into comercio values(7,'Iguana PC','Santos Vega 6006','B1682AIH','541569473274');
insert into comercio values(8,'Textil Hogar Villa Bosch','Gaucho Cruz 5481','B1682AKI','541148446790');
insert into comercio values(9,'Hotel Burbujas','Santos Vega 6357','B1657AJF','541156336881');
insert into comercio values(10,'Relojeria 1 De Mayo','1 de Mayo 1022','B1657BRH','541148480090');
insert into comercio values(11,'Mi Gusto','Serrano 1665','B1663GUK','541154664200');
insert into comercio values(12,'El Insumo SRL','Tucumán 2086','C1050AAP','541143723132');
insert into comercio values(13,'InterLiving','Av. Gral. Miguel de Azcuenaga 661','B1708ESI','541144200448');
insert into comercio values(14,'Lo de Carlitos','Coronel Charlone 1088','B1663NTU','541156672901');
insert into comercio values(15,'Las Margaritas','Av. Dr. Ricardo Balbín 1399','B1663NDA','541170868526');
insert into comercio values(16,'Mc Donalds','Av. Pres. Juan Domingo Perón 1522','B1663GHQ','1144551828'); --Sirve para testear funcion de compras por minuto 
insert into comercio values(17,'Nosh Up','Entre Ríos 698','B1661BUO','1120045123');
insert into comercio values(18,'Coto','Av. Pres. Hipólito Yrigoyen 1826' ,'B1665ABS','02320428689');
insert into comercio values(19,'Sarkany','Paunero 1479','B1663GJG','1146673212');
insert into comercio values(20,'Heladería Cremolatti','Av. Pres. Juan Domingo Perón 665','B1662ASG','1153505307');


--Cierre

insert into cierre values(2021,1,0,'2020-12-22','2021-01-20','2021-02-01'); 
insert into cierre values(2021,2,0,'2021-01-21','2021-02-22','2021-03-05'); 
insert into cierre values(2021,3,0,'2021-02-23','2021-03-25','2021-04-02'); 
insert into cierre values(2021,4,0,'2021-03-26','2021-04-25','2021-05-01');
insert into cierre values(2021,5,0,'2021-04-26','2021-05-24','2021-06-05');
insert into cierre values(2021,6,0,'2021-05-25','2021-06-22','2021-07-01');
insert into cierre values(2021,7,0,'2021-06-23','2021-07-20','2021-08-03');
insert into cierre values(2021,8,0,'2021-07-21','2021-08-23','2021-09-01');
insert into cierre values(2021,9,0,'2021-08-24','2021-09-26','2021-10-04');
insert into cierre values(2021,10,0,'2021-09-27','2021-10-26','2021-11-03');
insert into cierre values(2021,11,0,'2021-10-27','2021-11-29','2021-12-08');
insert into cierre values(2021,12,0,'2021-11-30','2021-12-31','2022-01-07');

insert into cierre values(2021,1,1,'2021-01-01','2021-01-30','2021-02-05');
insert into cierre values(2021,2,1,'2021-01-31','2021-02-28','2021-03-07');
insert into cierre values(2021,3,1,'2021-03-01','2021-03-30','2021-04-06');
insert into cierre values(2021,4,1,'2021-03-31','2021-04-30','2021-05-07');
insert into cierre values(2021,5,1,'2021-05-01','2021-06-01','2021-06-08');
insert into cierre values(2021,6,1,'2021-06-02','2021-07-02','2021-07-08');
insert into cierre values(2021,7,1,'2021-07-03','2021-08-03','2021-08-07');
insert into cierre values(2021,8,1,'2021-08-04','2021-09-01','2021-09-08');
insert into cierre values(2021,9,1,'2021-09-02','2021-09-29','2021-10-03');
insert into cierre values(2021,10,1,'2021-09-30','2021-10-26','2021-11-02');
insert into cierre values(2021,11,1,'2021-10-27','2021-11-29','2021-12-07');
insert into cierre values(2021,12,1,'2021-11-30','2021-12-31','2022-01-09');

insert into cierre values(2021,1,2,'2020-01-01','2021-01-31','2021-02-06');
insert into cierre values(2021,2,2,'2021-02-01','2021-03-01','2021-03-09');
insert into cierre values(2021,3,2,'2021-03-02','2021-03-30','2021-04-05');
insert into cierre values(2021,4,2,'2021-03-31','2021-04-28','2021-05-02');
insert into cierre values(2021,5,2,'2021-04-29','2021-05-27','2021-06-04');
insert into cierre values(2021,6,2,'2021-05-28','2021-06-28','2021-07-03');
insert into cierre values(2021,7,2,'2021-06-29','2021-07-30','2021-08-08');
insert into cierre values(2021,8,2,'2021-07-31','2021-08-27','2021-09-02');
insert into cierre values(2021,9,2,'2021-08-28','2021-09-24','2021-10-01');
insert into cierre values(2021,10,2,'2021-09-25','2021-10-26','2021-11-03');
insert into cierre values(2021,11,2,'2021-10-27','2021-11-28','2021-12-06');
insert into cierre values(2021,12,2,'2021-11-29','2021-12-29','2022-01-04');

insert into cierre values(2021,1,3,'2021-01-01','2021-01-31','2021-02-05');
insert into cierre values(2021,2,3,'2021-02-01','2021-02-28','2021-03-08');
insert into cierre values(2021,3,3,'2021-03-01','2021-03-31','2021-04-04');
insert into cierre values(2021,4,3,'2021-04-01','2021-04-29','2021-05-07');
insert into cierre values(2021,5,3,'2021-04-30','2021-05-30','2021-06-05');
insert into cierre values(2021,6,3,'2021-05-31','2021-06-30','2021-07-09');
insert into cierre values(2021,7,3,'2021-07-01','2021-07-30','2021-08-06');
insert into cierre values(2021,8,3,'2021-07-31','2021-08-28','2021-09-08');
insert into cierre values(2021,9,3,'2021-08-29','2021-09-30','2021-10-06');
insert into cierre values(2021,10,3,'2021-10-01','2021-10-31','2021-11-06');
insert into cierre values(2021,11,3,'2021-11-01','2021-11-29','2021-12-08');
insert into cierre values(2021,12,3,'2021-11-30','2021-12-31','2022-01-06');

insert into cierre values(2021,1,4,'2021-01-01','2021-01-31','2021-02-04');
insert into cierre values(2021,2,4,'2021-02-01','2021-02-28','2021-03-07');
insert into cierre values(2021,3,4,'2021-03-01','2021-03-31','2021-04-06');
insert into cierre values(2021,4,4,'2021-04-01','2021-04-29','2021-05-08');
insert into cierre values(2021,5,4,'2021-04-30','2021-05-30','2021-06-04');
insert into cierre values(2021,6,4,'2021-05-31','2021-06-30','2021-07-10');
insert into cierre values(2021,7,4,'2021-07-01','2021-07-30','2021-08-07');
insert into cierre values(2021,8,4,'2021-07-31','2021-08-28','2021-09-09');
insert into cierre values(2021,9,4,'2021-08-29','2021-09-30','2021-10-07');
insert into cierre values(2021,10,4,'2021-10-01','2021-10-31','2021-11-07');
insert into cierre values(2021,11,4,'2021-11-01','2021-11-29','2021-12-08');
insert into cierre values(2021,12,4,'2021-11-30','2021-12-31','2022-01-06');

insert into cierre values(2021,1,5,'2021-01-01','2021-01-31','2021-02-06');
insert into cierre values(2021,2,5,'2021-02-01','2021-02-28','2021-03-06');
insert into cierre values(2021,3,5,'2021-03-01','2021-03-30','2021-04-06');
insert into cierre values(2021,4,5,'2021-03-31','2021-04-30','2021-05-07');
insert into cierre values(2021,5,5,'2021-05-01','2021-05-30','2021-06-07');
insert into cierre values(2021,6,5,'2021-05-31','2021-06-29','2021-07-07');
insert into cierre values(2021,7,5,'2021-06-30','2021-07-29','2021-08-06');
insert into cierre values(2021,8,5,'2021-07-30','2021-08-28','2021-09-06');
insert into cierre values(2021,9,5,'2021-08-29','2021-09-27','2021-10-08');
insert into cierre values(2021,10,5,'2021-09-28','2021-10-30','2021-11-07');
insert into cierre values(2021,11,5,'2021-10-31','2021-11-30','2021-12-08');
insert into cierre values(2021,12,5,'2021-12-01','2021-12-31','2022-01-06');

insert into cierre values(2021,1,6,'2021-01-01','2021-01-30','2021-02-06');
insert into cierre values(2021,2,6,'2021-01-31','2021-02-28','2021-03-05');
insert into cierre values(2021,3,6,'2021-03-01','2021-03-31','2021-04-07');
insert into cierre values(2021,4,6,'2021-04-01','2021-04-30','2021-05-08');
insert into cierre values(2021,5,6,'2021-05-01','2021-05-29','2021-06-07');
insert into cierre values(2021,6,6,'2021-05-30','2021-06-29','2021-07-06');
insert into cierre values(2021,7,6,'2021-06-30','2021-07-31','2021-08-06');
insert into cierre values(2021,8,6,'2021-08-01','2021-08-31','2021-09-09');
insert into cierre values(2021,9,6,'2021-09-01','2021-09-30','2021-10-07');
insert into cierre values(2021,10,6,'2021-10-01','2021-10-31','2021-11-10');
insert into cierre values(2021,11,6,'2021-11-01','2021-11-30','2021-12-2');
insert into cierre values(2021,12,6,'2021-12-01','2021-12-31','2022-01-05');

insert into cierre values(2021,1,7,'2021-01-01','2021-01-31','2021-02-08');
insert into cierre values(2021,2,7,'2021-02-01','2021-02-28','2021-03-09');
insert into cierre values(2021,3,7,'2021-03-01','2021-03-31','2021-04-03');
insert into cierre values(2021,4,7,'2021-04-01','2021-04-30','2021-05-07');
insert into cierre values(2021,5,7,'2021-05-01','2021-05-31','2021-06-10');
insert into cierre values(2021,6,7,'2021-06-01','2021-06-30','2021-07-09');
insert into cierre values(2021,7,7,'2021-07-01','2021-07-31','2021-08-06');
insert into cierre values(2021,8,7,'2021-08-01','2021-08-31','2021-09-08');
insert into cierre values(2021,9,7,'2021-09-01','2021-09-30','2021-10-07');
insert into cierre values(2021,10,7,'2021-10-01','2021-10-31','2021-11-10');
insert into cierre values(2021,11,7,'2021-11-01','2021-11-30','2021-12-04');
insert into cierre values(2021,12,7,'2021-12-01','2021-12-31','2022-01-06');

insert into cierre values(2021,1,8,'2021-01-01','2021-01-31','2021-02-08');
insert into cierre values(2021,2,8,'2021-02-01','2021-02-28','2021-03-09');
insert into cierre values(2021,3,8,'2021-03-01','2021-03-31','2021-04-06');
insert into cierre values(2021,4,8,'2021-04-01','2021-04-30','2021-05-09');
insert into cierre values(2021,5,8,'2021-05-01','2021-05-31','2021-06-12');
insert into cierre values(2021,6,8,'2021-06-01','2021-06-30','2021-07-07');
insert into cierre values(2021,7,8,'2021-07-01','2021-07-31','2021-08-06');
insert into cierre values(2021,8,8,'2021-08-01','2021-08-31','2021-09-09');
insert into cierre values(2021,9,8,'2021-09-01','2021-09-30','2021-10-07');
insert into cierre values(2021,10,8,'2021-10-01','2021-10-31','2021-11-11');
insert into cierre values(2021,11,8,'2021-11-01','2021-11-30','2021-12-05');
insert into cierre values(2021,12,8,'2021-12-01','2021-12-31','2022-01-03');

insert into cierre values(2021,1,9,'2021-01-01','2021-01-31','2021-02-08');
insert into cierre values(2021,2,9,'2021-02-01','2021-02-28','2021-03-09');
insert into cierre values(2021,3,9,'2021-03-01','2021-03-31','2021-04-03');
insert into cierre values(2021,4,9,'2021-04-01','2021-04-30','2021-05-07');
insert into cierre values(2021,5,9,'2021-05-01','2021-05-31','2021-06-11');
insert into cierre values(2021,6,9,'2021-06-01','2021-06-30','2021-07-09');
insert into cierre values(2021,7,9,'2021-07-01','2021-07-31','2021-08-06');
insert into cierre values(2021,8,9,'2021-08-01','2021-08-31','2021-09-10');
insert into cierre values(2021,9,9,'2021-09-01','2021-09-30','2021-10-05');
insert into cierre values(2021,10,9,'2021-10-01','2021-10-31','2021-11-07');
insert into cierre values(2021,11,9,'2021-11-01','2021-11-30','2021-12-04');
insert into cierre values(2021,12,9,'2021-12-01','2021-12-31','2022-01-03');

--Consumos:


--consumos para generar resumen
insert into consumo values('1023202032800100','8163',3,800.0);
insert into consumo values('1023202032800100','8163',6,900.0);
insert into consumo values('1023202032800100','8163',7,500.0);


insert into consumo values('3248129374636441','2039',5,100.0);-- consumos en menos de 5 minutos distinto cod postal
insert into consumo values('3248129374636441','2039',10,10.0);
	
insert into consumo values('1023202032800100','8163',3,1000.0);  -- consumos en menos de un minuto mismo cod postal
insert into consumo values('1023202032800100','8163',16,30000.0);
	
insert into consumo values('5385923614372932','4871',10,60000.0); -- dos consumos con excesos de limite en el mismo dia
insert into consumo values('5385923614372932','4871',11,70000.0);
insert into consumo values('5385923614372932','4871',12,80000.0);

-- todos estos testean el triggerRechazo(se agregan a la tabla alerta automaticamente cuando entran el rechazo)
insert into consumo values('8274637103946203','1000',15,100.0); -- codigo invalido
	
insert into consumo values('4521563258974582','1265',18,200.0); -- tarjeta inexistente
insert into consumo values('6011408239546626','1763',15,500.0); -- tarjeta anulada
	
insert into consumo values('4030032322315337','7722',2,800.0); -- tarjeta suspendida
	
insert into consumo values('2001445592130048','5377',5,51000.0); -- supera limite
	
insert into consumo values('3423456779756435','1214',11,50.0); -- tarjeta vencida

--CREAR FUNCIONES

\i tarjetaNoValida.sql
\i codInvalido.sql
\i montoNoPermitido.sql
\i tarjetaSuspendida.sql
\i tarjetaVencida.sql
\i triggerRechazo.sql
\i triggerComprasEnUnMinuto.sql
\i triggerComprasEnCincoMinutos.sql
\i autorizacionDeCompra.sql
\i alertaRechazosDiarios.sql
\i eliminarpks.sql
\i generarResumen.sql
\c postgres
