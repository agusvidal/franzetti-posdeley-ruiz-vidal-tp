create or replace function montoNoPermitido(nro_tarj varchar(16),monto_a_pagar decimal(7,2))returns boolean as $$
declare

limite decimal(8,2);
pendientes_de_pago decimal(8,2);

begin

select limite_compra into limite from tarjeta where  tarjeta.nro_tarjeta  = nro_tarj ;
select sum(monto) into pendientes_de_pago from compra where pagado = false; 

if pendientes_de_pago is null then
	pendientes_de_pago := 0.0;
end if;

if pendientes_de_pago+monto_a_pagar > limite then

	return true;

else

	return false;

end if;

end;
$$ language plpgsql;
