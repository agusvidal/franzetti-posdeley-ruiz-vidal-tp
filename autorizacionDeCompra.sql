
create or replace function autorizacionDeCompra(nro_tarj varchar(16),cod varchar(4), nro_comercio int, monto decimal(8,2))returns boolean as $$
declare

pk_rechazo int;
nro_compra int;
tarjeta_suspendida_ok bool;
monto_ok bool;
codigo_ok bool;
tarjeta_invalida_ok bool;
tarjeta_vencida_ok bool;
begin

select count (*) into pk_rechazo from rechazo;
pk_rechazo := pk_rechazo+1;
select count (*) into nro_compra from compra;
nro_compra := nro_compra +1;

select tarjetaSuspendida(nro_tarj) into tarjeta_suspendida_ok;
select montoNoPermitido(nro_tarj, monto) into monto_ok;
select codInvalido (nro_tarj, cod) into codigo_ok;
select tarjetaNoValida (nro_tarj) into tarjeta_invalida_ok;
select tarjetaVencida (nro_tarj) into tarjeta_vencida_ok;


if tarjeta_vencida_ok then 
	insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'Plazo de vigencia expirado');
	return false;
end if;

if tarjeta_suspendida_ok then
	insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'La tarjeta se encuentra suspendida');
	return false;
end if;

if monto_ok then
	insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'Supera el limite de tarjeta');
	return false;
end if;

if codigo_ok then 
	insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'Codigo de seguridad invalido');
	return false;
end if;

if tarjeta_invalida_ok then 
	insert into rechazo values(pk_rechazo,nro_tarj,nro_comercio,current_timestamp,monto,'Tarjeta no valida o no vigente');
	return false;
end if;

	insert into compra values(nro_compra,nro_tarj,nro_comercio,current_timestamp, monto, false);
	return true;

end;

$$ language plpgsql;
