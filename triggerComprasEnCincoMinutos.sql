create or replace function triggerComprasEnCincoMinutos() returns trigger as $$

declare

pk_alerta int;
segundos_diferencias int;
fecha_actual timestamp;
compras int;

begin 
	
	
	select count(nro_alerta) into pk_alerta from alerta;
	pk_alerta := pk_alerta + 1;
	fecha_actual:= new.fecha;
	
	
	select count (distinct (comercio.codigo_postal)) into compras from comercio where comercio.codigo_postal in (select comercio.codigo_postal from compra,comercio where compra.nro_comercio = comercio.nro_comercio and compra.nro_tarjeta = new.nro_tarjeta and 
	extract(year from fecha_actual) = extract(year from fecha) and extract(month from fecha_actual) = extract(month from fecha) and 
	extract(day from fecha_actual)= extract(day from fecha) and new.nro_tarjeta = compra.nro_tarjeta and ((extract (hour from fecha_actual)*3600 + extract (minute from fecha_actual)*60+ extract(second from fecha_actual))-(extract (hour from fecha)*3600+ extract(minute from fecha)*60 + extract(second from fecha)))<301 group by comercio.codigo_postal having count(*) = 1);
	

	if compras>1 then
		insert into alerta values(pk_alerta, new.nro_tarjeta, new.fecha, null, 5, 'se hizo mas de una compra en cinco minutos');
	end if;
		return new; 	
		
end;                                                                    
$$ language plpgsql;                                                    
                                                                        
create trigger comprasProximasACincoMinutos_trg                                              
after insert on compra                                                
for each row                                                            
execute procedure triggerComprasEnCincoMinutos();
