create or replace function triggerRechazo() returns trigger as $$

declare

pk_alerta int;

begin 
	
	select count(nro_alerta) into pk_alerta from alerta;
	pk_alerta := pk_alerta + 1;
	
	
	insert into alerta values(pk_alerta,new.nro_tarjeta,new.fecha,new.nro_rechazo,0,new.motivo);
	return new;

end;
$$ language plpgsql;

create trigger rechazo_trg
before insert on rechazo
for each row
execute procedure triggerRechazo();
